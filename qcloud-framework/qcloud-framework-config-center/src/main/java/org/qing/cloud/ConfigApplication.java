package org.qing.cloud;

import org.qing.cloud.commons.logs.log4j2.Log4j2StartedEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@EnableAutoConfiguration
@EnableDiscoveryClient
@EnableConfigServer
@SpringBootApplication
public class ConfigApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ConfigApplication.class, args);
		SpringApplication app = new SpringApplication(ConfigApplication.class);
        app.addListeners(new Log4j2StartedEventListener());
        app.run(args);
	}
	
	@Configuration
	class WebMvcJSONConfig extends WebMvcConfigurationSupport {
		@Override
	    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	      configurer.ignoreAcceptHeader(true).defaultContentType(MediaType.APPLICATION_JSON);
	    }
	}
}
