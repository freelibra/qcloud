package org.qing.cloud.commons.common.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 公共Entity
 * @author qing
 *
 */
@Getter
@Setter
@SuppressWarnings("serial")
public class Entity implements Serializable{
	
	/**
	 * 主键
	 */
	private Long id;
	
	/**
	 * 备注
	 */
	private String remarks;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 程序变更时间
	 */
	private Date updateTime;
	
	/**
	 * 数据库变更时间
	 */
	private Date dbUpdateTime;
	
	/**
	 * 删除状态
	 */
	private Boolean deleteFlg;
	
}
