package org.qing.cloud.commons.utils.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.springframework.util.ObjectUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * Jackson
 * 
 * @author qing
 *
 */
public class JSON {

	private static ObjectMapper objectMapper = new ObjectMapper();

	static {
		// 设置Jackson序列化时只包含不为空的字段
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		// 设置在反序列化时忽略在JSON字符串中存在，而在Java中不存在的属性
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		// 序列化将Long转String类型
		SimpleModule longModule = new SimpleModule();
		longModule.addSerializer(Long.class, ToStringSerializer.instance);
		longModule.addSerializer(Long.TYPE, ToStringSerializer.instance);

		// 序列化将BigInteger转String类型
		SimpleModule bigIntegerModule = new SimpleModule();
		bigIntegerModule.addSerializer(BigInteger.class, ToStringSerializer.instance);

		// 序列化将BigDecimal转String类型
		SimpleModule bigDecimalModule = new SimpleModule();
		bigDecimalModule.addSerializer(BigDecimal.class, ToStringSerializer.instance);

		objectMapper.registerModule(longModule);
		objectMapper.registerModule(bigIntegerModule);
		objectMapper.registerModule(bigDecimalModule);
	}

	public static ObjectMapper getObjectMapper() {
		return JSON.objectMapper;
	}

	public static void setObjectMapper(ObjectMapper objectMapper) {
		JSON.objectMapper = objectMapper;
	}

	public static String toJSONString(Object obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static <T> T parseObject(String json, Class<T> clazz) {
		T object = null;
		try {
			if (!ObjectUtils.isEmpty(json)) {
				object = objectMapper.readValue(json, new TypeReference<T>() {
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	public static <T> List<T> parseArray(String json, Class<T> clazz) {
		List<T> list = null;
		try {
			if (!ObjectUtils.isEmpty(json)) {
				list = objectMapper.readValue(json, new TypeReference<List<T>>() {
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static String toJSONString(ObjectMapper objectMapper, Object obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static <T> T parseObject(ObjectMapper objectMapper, String json, Class<T> clazz) {
		T object = null;
		try {
			if (!ObjectUtils.isEmpty(json)) {
				object = objectMapper.readValue(json, new TypeReference<T>() {
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	public static <T> List<T> parseArray(ObjectMapper objectMapper, String json, Class<T> clazz) {
		List<T> list = null;
		try {
			if (!ObjectUtils.isEmpty(json)) {
				list = objectMapper.readValue(json, new TypeReference<List<T>>() {
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
