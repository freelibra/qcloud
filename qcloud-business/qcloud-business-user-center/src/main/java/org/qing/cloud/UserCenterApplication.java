package org.qing.cloud;

import org.qing.cloud.commons.logs.log4j2.Log4j2StartedEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class UserCenterApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(UserCenterApplication.class);
        app.addListeners(new Log4j2StartedEventListener());
        app.run(args);
	}
}
