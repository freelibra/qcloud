package org.qing.cloud.user.service;

import org.qing.cloud.user.entity.User;

import reactor.core.publisher.Flux;

public interface UserService {

	Flux<User> findByName(String name);

	Flux<User> findAll();

}
