package org.qing.cloud.user.service.impl;

import org.qing.cloud.user.dao.UserDao;
import org.qing.cloud.user.entity.User;
import org.qing.cloud.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Override
	public Flux<User> findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	@CachePut(value = "cacheKey", key = "'test'")
	public Flux<User> findAll() {
		return dao.findAll();
	}
}
