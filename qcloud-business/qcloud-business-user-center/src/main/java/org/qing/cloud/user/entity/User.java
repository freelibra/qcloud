package org.qing.cloud.user.entity;

import org.qing.cloud.commons.common.entity.Entity;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Table("tb_user")
@Accessors(chain = true)
@SuppressWarnings("serial")
public class User extends Entity{
	
	/**
	 * 名称
	 */
	private String name;
	
}
